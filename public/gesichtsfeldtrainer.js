const FLASH_WIDTH = 20;
const FLASH_HEIGHT = 20;

class Blink {

  color = 'rgb(255,255,255)';

  #context = null;
  #topX = 0;
  #topY = 0;

  #caught = false;

  get caught() {
    return this.#caught;
  }

  constructor(context) {
    this.#context = context;
    const canvas = context.canvas;

    this.#topX = Math.min(Math.max(0, (Math.random() * canvas.width)), canvas.width - FLASH_WIDTH);
    this.#topY = Math.min(Math.max(0, (Math.random() * canvas.height)), canvas.height - FLASH_HEIGHT);

    this.onKeyDown = this.onKeyDown.bind(this);
  }

  show() {
    this.#draw(this.color, this.#topX, this.#topY, FLASH_WIDTH, FLASH_HEIGHT);
  }

  hide() {
    const backColor = window.getComputedStyle(this.#context.canvas, null).getPropertyValue('background-color');
    // need a larger rectangle to "hide" the blink; without this a shadow may remain
    this.#draw(backColor, this.#topX - 1, this.#topY - 1, FLASH_WIDTH + 2, FLASH_HEIGHT + 2);
  }

  #draw(color, topX, topY, width, height) {
    this.#context.fillStyle = color;
    this.#context.fillRect(topX, topY, width, height);
  }

  onKeyDown(e) {
    this.#caught = true;
    this.#draw('rgb(0,255,0)', this.#topX, this.#topY, FLASH_WIDTH, FLASH_HEIGHT);
  }
}

class Target {
  #context = null;

  constructor(context) {
    this.#context = context;
  }

  show() {
    this.#draw('rgb(255,255,255)');
  }

  hide() {
    this.#draw(window.getComputedStyle(this.#context.canvas, null).getPropertyValue('background-color'));
  }

  #draw(color) {
    this.#context.fillStyle = color;
    this.#context.beginPath();
    this.#context.arc(this.#context.canvas.width / 2, this.#context.canvas.height / 2, 5, 0, 2 * Math.PI, false);
    this.#context.fill();
  }
}

var target = null;
var loops = 30;
var blinks = new Array();
var debug = false;

function start() {

  const canvas = document.getElementById("main");
  canvas.width = document.body.clientWidth;
  canvas.height = document.body.clientHeight;

  const urlParams = new URL(window.location.href).searchParams;
  loops = urlParams.get("loops") == null ? loops : urlParams.get("loops");
  debug = urlParams.get("debug") == null ? false : urlParams.get("debug");
  
  if(debug) {
    var debugConsole = document.getElementById("debugConsole");
    debugConsole.style.display = 'block';
    debugConsole.innerHTML = "Canvas Size="+canvas.width + "x" + canvas.height;
  }

  if (canvas.getContext) {
    const ctx = canvas.getContext('2d');
    target = new Target(ctx);
    target.show();
    window.setTimeout(drawScreen, 3000, ctx);
  }
}

function drawScreen(ctx) {

  if (blinks.length > 0) {
    const prevBlink = blinks[blinks.length - 1];
    prevBlink.hide();
    window.removeEventListener('keydown', prevBlink.onKeyDown);
  }

  if (blinks.length == loops) {
    ctx.fillStyle = 'lightgrey';
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    var caughtCount = 0;
    blinks.forEach((blink) => {
      blink.color = blink.caught ? 'green' : 'darkgrey';
      caughtCount += blink.caught ? 1 : 0;
      blink.show();
    });

    alert(caughtCount + " von " + blinks.length + " getroffen");
    
    window.addEventListener('keydown', (e) => {
      window.location.reload();
    }, {once:true});
    
  } else {
    const newBlink = new Blink(ctx);
    blinks.push(newBlink);
    newBlink.show();
    window.addEventListener('keydown', newBlink.onKeyDown);

    target.show();

    window.setTimeout(drawScreen, 1000 + Math.random() * 2000, ctx);
  }
}